package string_calculator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import string_calculator.input_validation.StringNumberValidator;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StringCalculatorTest {
    private StringCalculator calculator;

    @BeforeEach
    public void setup() {
        final StringNumberValidator validator = s -> {};
        calculator = new StringCalculator(validator);
    }

    @Test
    public void testSmallNumbersWithDifferentLength() {
        final String s1 = "123";
        final String s2 = "11";
        final String expected = "134";

        assertEquals(expected, calculator.addNumbers(s1, s2));
        assertEquals(expected, calculator.addNumbers(s2, s1));
    }

    @Test
    public void testSmallNumbersWithSameLength() {
        final String s1 = "4053";
        final String s2 = "1204";
        final String expected = "5257";

        assertEquals(expected, calculator.addNumbers(s1, s2));
        assertEquals(expected, calculator.addNumbers(s2, s1));
    }

    @Test
    public void testAdditionWithCarryOverOnTheRight() {
        final String s1 = "148";
        final String s2 = "13";
        final String expected = "161";

        assertEquals(expected, calculator.addNumbers(s1, s2));
        assertEquals(expected, calculator.addNumbers(s2, s1));
    }

    @Test
    public void testAdditionWithCarryOverInTheMiddleSameLength() {
        final String s1 = "748";
        final String s2 = "171";
        final String expected = "919";

        assertEquals(expected, calculator.addNumbers(s1, s2));
        assertEquals(expected, calculator.addNumbers(s2, s1));
    }

    @Test
    public void testAdditionWithCarryOverInTheMiddleDifferentLength() {
        final String s1 = "748";
        final String s2 = "71";
        final String expected = "819";

        assertEquals(expected, calculator.addNumbers(s1, s2));
        assertEquals(expected, calculator.addNumbers(s2, s1));
    }

    @Test
    public void testAdditionWithCarryOverOnTheLeft() {
        final String s1 = "748";
        final String s2 = "521";
        final String expected = "1269";

        assertEquals(expected, calculator.addNumbers(s1, s2));
        assertEquals(expected, calculator.addNumbers(s2, s1));
    }

    @Test
    public void testMultipleIsolatedCarryOvers() {
        final String s1 = "108356";
        final String s2 = "3624";
        final String expected = "111980";

        assertEquals(expected, calculator.addNumbers(s1, s2));
        assertEquals(expected, calculator.addNumbers(s2, s1));
    }

    @Test
    public void testMultipleIsolatedCarryOversWithExtendedPropagation() {
        final String s1 = "108356";
        final String s2 = "3724";
        final String expected = "112080";

        assertEquals(expected, calculator.addNumbers(s1, s2));
        assertEquals(expected, calculator.addNumbers(s2, s1));
    }

    @Test
    public void testCarryOverWithPropagationFromRightToLeft() {
        final String s1 = "999999999";
        final String s2 = "999";
        final String expected = "1000000998";

        assertEquals(expected, calculator.addNumbers(s1, s2));
        assertEquals(expected, calculator.addNumbers(s2, s1));
    }

    @Test
    public void testAdditionOfBigNumbers() {
        final String s1 = "123456789012345678901234567890";
        final String s2 = "34789165423504563214878965412645";
        final String expected = "34912622212516908893780199980535";

        assertEquals(expected, calculator.addNumbers(s1, s2));
        assertEquals(expected, calculator.addNumbers(s2, s1));
    }

    @Test
    public void testAdditionOfBigNumbers2() {
        final String s1 = "123456789012345678901";
        final String s2 = "12345678";
        final String expected = "123456789012358024579";

        assertEquals(expected, calculator.addNumbers(s1, s2));
        assertEquals(expected, calculator.addNumbers(s2, s1));
    }

    @Test
    public void testAdditionOfZero() {
        final String s1 = "0";
        final String s2 = "0";
        final String expected = "0";

        assertEquals(expected, calculator.addNumbers(s1, s2));
        assertEquals(expected, calculator.addNumbers(s2, s1));
    }
}