package string_calculator.input_validation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class StringNumberValidatorImpTest {
    private StringNumberValidatorImp validator;

    @BeforeEach
    public void setup() {
        validator = new StringNumberValidatorImp();
    }

    @Test
    public void testRejectNullString() {
        final StringNumberException sne = assertThrows(StringNumberException.class, () -> validator.validate(null));
        assertEquals("Null input", sne.getMessage());
    }

    @Test
    public void testRejectInvalidCharacter() {
        final String input = "510b31";
        final StringNumberException sne = assertThrows(StringNumberException.class, () -> validator.validate(input));
        assertEquals("Invalid character at index 3: [b]. Input: 510b31", sne.getMessage());
    }

    @Test
    public void testRejectInvalidCharacterOfBigString() {
        final String input = "412360255674123448962546-";
        final StringNumberException sne = assertThrows(StringNumberException.class, () -> validator.validate(input));
        assertEquals("Invalid character at index 24: [-]. Input: 4123602556...", sne.getMessage());
    }

    @Test
    public void testPassValidNumberWithEveryDigit() {
        final String input = "1234567890";
        validator.validate(input);
    }
}