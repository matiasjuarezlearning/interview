package string_calculator;

import string_calculator.input_validation.StringNumberValidator;

public class StringCalculator {
    private final StringNumberValidator stringNumberValidator;

    public StringCalculator(final StringNumberValidator stringNumberValidator) {
        this.stringNumberValidator = stringNumberValidator;
    }

    public String addNumbers(final String s1, final String s2) {
        this.stringNumberValidator.validate(s1);
        this.stringNumberValidator.validate(s2);

        final int s1l = s1.length();
        final int s2l = s2.length();
        final int minl = Integer.min(s1l, s2l);
        final int maxl = Integer.max(s1l, s2l);
        final String biggerString = s1l > s2l ? s1 : s2;

        StringBuilder result = new StringBuilder(maxl + 1);

        int previousCarry = 0;

        for (int i = 1; i <= maxl; i++) {
            final int currentAddition;
            if (i <= minl) {
                currentAddition = (s1.charAt(s1l - i) + s2.charAt(s2l - i)) - ('0' * 2) + previousCarry;
            } else {
                currentAddition = biggerString.charAt(maxl - i) - '0' + previousCarry;
            }

            final int currentDigit = currentAddition % 10;
            previousCarry = currentAddition / 10;

            result.append(currentDigit);
        }

        if (previousCarry > 0) {
            result.append(previousCarry);
        }

        return result.reverse().toString();
    }
}
