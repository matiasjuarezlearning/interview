package string_calculator.input_validation;

public class StringNumberException extends RuntimeException {
    public StringNumberException(String message) {
        super(message);
    }
}
