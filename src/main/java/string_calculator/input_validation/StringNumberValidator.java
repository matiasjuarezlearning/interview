package string_calculator.input_validation;

public interface StringNumberValidator {
    void validate(final String s);
}
