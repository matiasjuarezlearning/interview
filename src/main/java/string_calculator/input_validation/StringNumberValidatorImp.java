package string_calculator.input_validation;

public class StringNumberValidatorImp implements StringNumberValidator {
    @Override
    public void validate(final String s) {
        if (s == null) {
            throw new StringNumberException("Null input");
        }

        char nextChar;
        for (int i = 0; i < s.length(); i++) {
            nextChar = s.charAt(i);
            if (nextChar < '0' || nextChar > '9') {
                final String firstChars = s.substring(0, Integer.min(10, s.length()));
                final String formattedMsgString = firstChars.length() == s.length() ?
                        s : (firstChars + "...");

                final String msg = String.format(
                        "Invalid character at index %s: [%s]. Input: %s",
                        i, nextChar, formattedMsgString);

                throw new StringNumberException(msg);
            }
        }
    }
}
